﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public CharacterStats characterStats;
	public int CurrentHealth,MaxHealth,Level,Exp,ExpToLvlUp,AttributePoints;	
	private readonly int[] levelUpLimits=new int []{100,200,400,800,1600,3200,6400}; 
	private RectTransform characterPanel;
	
	// Use this for initialization
	void Start () {
		CombatEvents.OnEnemyDeath+=GetKilledEnemyExp;
		EventManager.OnQuestCompleted+=GetCompletedQuestExp;
		this.CurrentHealth=this.MaxHealth;
		this.Level=1;
		this.Exp=0;	
		this.ExpToLvlUp=levelUpLimits[Level-1];
		characterStats=new CharacterStats(10,10,10);		
		UIEventHandler.UpdatePlayerStats();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void TakeDamage(int amount){
		Debug.Log("taking damage:"+amount);
		CurrentHealth-=amount;		
		if(CurrentHealth<=0){
			Die();	
		}
		UIEventHandler.UpdatePlayerStats();
	}
	void Die(){
		Debug.Log("You Died.!. resetting the health.");
		this.CurrentHealth=this.MaxHealth;
		UIEventHandler.UpdatePlayerStats();
	}
	void LevelUp(){
		while(Exp>=ExpToLvlUp)
		{
			Level++;
			AttributePoints+=3;
			Exp-=ExpToLvlUp;
			ExpToLvlUp=levelUpLimits[Level-1];
		}				
	}
	void GainExp(int amount){
		Exp+=amount;
		if(Exp>=ExpToLvlUp){
			LevelUp();			
		}
		UIEventHandler.UpdatePlayerStats();
	}
	void GetKilledEnemyExp(IEnemy enemy){
		GainExp(enemy.Experience);
	}
	void GetCompletedQuestExp(Quest quest){
		GainExp(quest.ExpReward);
	}
}
