﻿

public interface IEnemy  {
	int ID { get; set; }
	EnemySpawner enemySpawner {get;set;}
	int Experience { get; set; }
	void Die();
	void TakeDamage(int damageAmount);
	void PerformAttack();
	
	

}
