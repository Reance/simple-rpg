﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : Interactable {

    public string[] Dialogue;
    public string Name;
    
    public override void Interact()
    {
        DialogueManager.Instance.AddNewDialogue(Dialogue,Name);
        Debug.Log("interacting with  NPC.");
    }
}
