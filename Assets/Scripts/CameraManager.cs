﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {		
	public Transform CameraTarget;
	public float PlayerCameraDistance{get;set;}
	Camera PlayerCamera;
	float ZoomSpeed=30f;
	// Use this for initialization
	void Start () {
		PlayerCameraDistance=12f;
		PlayerCamera=GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetAxisRaw("Mouse ScrollWheel")!=0){
			float scroll=Input.GetAxis("Mouse ScrollWheel");
			PlayerCamera.fieldOfView -= scroll*ZoomSpeed;
			PlayerCamera.fieldOfView=Mathf.Clamp(PlayerCamera.fieldOfView,15,100);
		}
		transform.position=new Vector3(CameraTarget.position.x,CameraTarget.position.y+PlayerCameraDistance,CameraTarget.position.z+PlayerCameraDistance);
	}
}
