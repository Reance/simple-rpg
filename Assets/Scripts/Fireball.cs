﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour {

	public Vector3 Direction { get; set; }
	public float Range { get; set; }
	public int  Damage { get; set; }
	Vector3 spawnPoint;
	
	void Start()
	{
		Range=30f;
		Damage=4;
		spawnPoint=transform.position;
		GetComponent<Rigidbody>().AddForce(Direction * 100f);
	}
	private void Update() {
		if(Vector3.Distance(spawnPoint,transform.position)>=Range){
			Extinguish();
		}
	}
	void Extinguish(){
		Destroy(this.gameObject);
	}

	 void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.tag=="Enemy"){
			other.transform.GetComponent<IEnemy>().TakeDamage(Damage);
		}
		Extinguish();
	}
}
