﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Yourself : MonoBehaviour,IEnemy {
public LayerMask aggroLayerMask;
public float Current_Health,Max_Health;
public int Experience { get; set; }
public int ID { get; set; }

public ItemDropTable DropTable {get;set;}
public EnemySpawner enemySpawner { get; set; }

public PickupItem pickupItem;

private NavMeshAgent navAgent;
private Player player;
private Collider []CollidersInsideAggroArea;
private CharacterStats characterStats;
void Start()
{
	DropTable=new ItemDropTable();
	DropTable.LootDrops=new List<Loot>
	{
		new Loot("sword",25),
		new Loot("staff",25),
		new Loot("potion",25)		
	};
	ID=0;
	navAgent=GetComponent<NavMeshAgent>();
	characterStats=new CharacterStats(6,10,2);
	Current_Health=Max_Health;
	Experience=50;
}
void FixedUpdate() {
	CollidersInsideAggroArea=Physics.OverlapSphere(transform.position,15f,aggroLayerMask);
	if(CollidersInsideAggroArea.Length>0){
		//only collider that have the player layermask is player so first element of the array is the player..
		chasePlayer(CollidersInsideAggroArea[0].GetComponent<Player>());
	}
}

	public void TakeDamage(int damageAmount){
		Current_Health-=damageAmount;
		if(Current_Health<=0){
			Die();	
		}
	}
	void chasePlayer(Player player){
		this.player=player;
		navAgent.SetDestination(player.transform.position);
		if(navAgent.remainingDistance<=navAgent.stoppingDistance){
			if(!IsInvoking("PerformAttack"))
				InvokeRepeating("PerformAttack",.5f,2f);
		}else{
				
				//if(IsInvoking("PerformAttack"))
				CancelInvoke("PerformAttack");
		}
		
		
	}
	public void PerformAttack(){
		this.player.TakeDamage(characterStats.GetStat(BaseStat.BaseStatTypes.Power).BaseValue);
	}
	public void Die(){
		Destroy(this.gameObject);
		CombatEvents.EnemyDied(this);
		dropLoot();
		this.enemySpawner.Respawn();
		Debug.Log("Congratz!!! You Just Lift A Barrier To Achive Your LifeGoals !.."+
		"BTW that dude's health was below zero so it just vanished."+" Dont worry it will come back in a time ;) ");
	}
	void dropLoot(){
		Item item=DropTable.GetDrop();
		if(item!=null){
			PickupItem instance=Instantiate(pickupItem,transform.position,Quaternion.identity);
			instance.DropItem=item;
			Debug.Log("Item Dropped: "+instance.DropItem.ItemName);
		}
	}
}
