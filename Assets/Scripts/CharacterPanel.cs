﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CharacterPanel : MonoBehaviour {
	
	[SerializeField] private Text level,health;
	[SerializeField] private Image healthBar,levelBar;
	[SerializeField] private Player player;
	[SerializeField] private Text playerStatPrefab;
	[SerializeField] private Transform playerStatsPanel;
	private List<Text> playerStatTexts=new List<Text>();
	// equipped weapon
	[SerializeField] private Sprite defaultWeaponSprite;
	private PlayerWeaponManager playerWeaponManager;
	[SerializeField]  private Text weaponStatPrefab;
	[SerializeField] private Transform weaponStatPanel;
	[SerializeField] private Image weaponImage;
	[SerializeField] private Text weaponName;
	private List<Text> weaponStatTexts=new List<Text>();
	
	// Use this for initialization
	void Start () {
		playerWeaponManager=player.GetComponent<PlayerWeaponManager>();
	}
	private void Awake() {

		UIEventHandler.OnUpdatePlayerStats+=updatePlayerStats;
		UIEventHandler.OnItemEquipped+=updateEquippedWeapon; 
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void updatePlayerStats(){		
		this.level.text=player.Level.ToString();
		this.levelBar.fillAmount=(float)player.Exp/(float)player.ExpToLvlUp;
		this.health.text=player.CurrentHealth.ToString();
		this.healthBar.fillAmount=(float)player.CurrentHealth/(float)player.MaxHealth;
		
		initPlayerStats();
		//Debug.Log("player power: "+player.characterStats.GetStat(BaseStat.BaseStatTypes.Power).GetModifiedStatValue()+"\n"+
		//"player vitality: "+player.characterStats.GetStat(BaseStat.BaseStatTypes.Vitality).GetModifiedStatValue()+"\n"+
		//"player attackspeed: "+player.characterStats.GetStat(BaseStat.BaseStatTypes.AttackSpeed).GetModifiedStatValue());
	}
	void updateEquippedWeapon(Item item){
		weaponImage.sprite=Resources.Load<Sprite>("UI/Icons/Items/"+item.ItemID);
		weaponName.text=item.ItemName;
		initWeaponStats(item);

	}
	void initPlayerStats(){
		clearPlayerStatTexts();
		for(int i=0;i<player.characterStats.Stats.Count;i++){
			playerStatTexts.Add(Instantiate(playerStatPrefab));
			playerStatTexts[i].transform.SetParent(playerStatsPanel);
			playerStatTexts[i].text=player.characterStats.Stats[i].StatName +": "+player.characterStats.Stats[i].GetModifiedStatValue().ToString();
		}
	}
	void clearPlayerStatTexts(){
		for(int i=0;i<playerStatTexts.Count;i++){
			Destroy(playerStatTexts[i].gameObject);
		}
		playerStatTexts.Clear();
	}
		void initWeaponStats(Item item){
		clearWeaponStatTexts();
		for(int i=0;i<item.Stats.Count;i++){
			weaponStatTexts.Add(Instantiate(weaponStatPrefab));
			weaponStatTexts[i].transform.SetParent(weaponStatPanel);
			weaponStatTexts[i].text=item.Stats[i].StatName +": "+item.Stats[i].BaseValue.ToString();
		}
	}
	void clearWeaponStatTexts(){
		for(int i=0;i<weaponStatTexts.Count;i++){
			Destroy(weaponStatTexts[i].gameObject);
		}
		weaponStatTexts.Clear();
	}
	public void UnequipWeapon(){
		weaponName.text="-";
		weaponImage.sprite=defaultWeaponSprite;
		clearWeaponStatTexts();
		playerWeaponManager.UnequipWeapon();
	}
	
}
