﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIEventHandler : MonoBehaviour {

public delegate void ItemEventHandler(Item item);
public static event ItemEventHandler OnItemAddedToInventory;
public static event ItemEventHandler OnItemEquipped;

public delegate void StatEventHandler();
public static event StatEventHandler OnUpdatePlayerStats;
public static void ItemAddedToInventory(Item item){
	OnItemAddedToInventory(item);
}
public static void ItemEquipped(Item item){
	OnItemEquipped(item);
}
public static void UpdatePlayerStats(){
	OnUpdatePlayerStats();
}
}
