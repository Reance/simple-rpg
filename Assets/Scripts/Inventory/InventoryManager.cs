﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InventoryManager : MonoBehaviour {
	public static InventoryManager Instance {get;set;}
	public PlayerWeaponManager playerWeaponManager;
	public ConsumableManager consumableManager;
	public InventoryDetailsUI inventoryDetailsUI;
	public List<Item> PlayerItems =new List<Item>();
 void Start() {
	if(Instance!=null && Instance != this)
	{
		Destroy(gameObject);
	}else{
		Instance=this;
	}
	 consumableManager=GetComponent<ConsumableManager>();
	 playerWeaponManager=GetComponent<PlayerWeaponManager>();
	 AddItem("sword");
	 AddItem("potion");
	 AddItem("staff");
}
	public void AddItem(string itemID){
		Item item =ItemDatabase.Instance.GetItem(itemID);
		PlayerItems.Add(item);
		UIEventHandler.ItemAddedToInventory(item);		
	}
	public void AddItem(Item item){		
		PlayerItems.Add(item);
		UIEventHandler.ItemAddedToInventory(item);		
	}
	
	public void SetItemDetails(Item item,Button selectedButton){
		inventoryDetailsUI.SetItem(item,selectedButton); 
	}
	public void EquipItem(Item itemToEquip){
		playerWeaponManager.EquipWeapon(itemToEquip);
	}
	public void ConsumeItem(Item itemToConsume){
		consumableManager.ConsumeItem(itemToConsume);
	}
}
