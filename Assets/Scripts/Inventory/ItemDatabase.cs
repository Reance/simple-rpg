﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
public class ItemDatabase : MonoBehaviour
{
    public static ItemDatabase Instance{get;set;}
    private List<Item>items{get;set;}
    void Awake()
    {
        if(Instance!=null && Instance!=this)
            Destroy(gameObject);
        else
            Instance=this;
        BuildDatabase();   
    }
    private void BuildDatabase(){
        items=JsonConvert.DeserializeObject<List<Item>>(Resources.Load<TextAsset>("Json/Items").ToString());        
    }

    public Item GetItem(string itemID){
        foreach(Item item in items){
            if(item.ItemID==itemID)
                return item;
            
        }
        Debug.Log("Could not find the "+itemID);
        return null;
    }
	
}
