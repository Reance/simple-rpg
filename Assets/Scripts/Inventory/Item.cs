﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class Item {
	public enum ItemTypes{Weapon,Consumable,Quest}
	public List<BaseStat> Stats {get;set;}	
	[JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
	public ItemTypes ItemType{get;set;}
	public string Description { get; set; }
	public string ActionName { get; set; }
	public bool isModifier { get; set; }
	public string ItemName { get; set; }
	public string ItemID {get;set;}

	public Item(List<BaseStat> _stats,string _itemID){
		this.Stats=_stats;
		this.ItemID=_itemID;
	}
	[Newtonsoft.Json.JsonConstructor]
	public Item(List<BaseStat> _stats,string _itemID,ItemTypes _ItemType,string _Description,string _ActionName,bool _isModifier,string _ItemName){
		this.Stats=_stats;
		this.ItemID=_itemID;
		this.Description=_Description;
		this.ActionName=_ActionName;
		this.isModifier=_isModifier;
		this.ItemName=_ItemName;
		this.ItemType=_ItemType;
	}
}
