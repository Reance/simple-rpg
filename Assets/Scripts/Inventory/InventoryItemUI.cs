﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InventoryItemUI : MonoBehaviour {

	public Item item;
	public Text itemText;
	public Image itemImage;
	public void SetItem(Item item)
	{
		this.item=item;
		SetupItemValues();
	}
	void SetupItemValues(){

		itemText.text=this.item.ItemName;
		itemImage.sprite=Resources.Load<Sprite>("UI/Icons/Items/"+item.ItemID); 
	}
	public void OnSelectItemButton(){
		InventoryManager.Instance.SetItemDetails(item,GetComponent<Button>());
	}
}
