﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUI : MonoBehaviour {

public RectTransform InventoryPanel;
public RectTransform ScrollviewContent;
InventoryItemUI ItemConteiner{get;set;}
bool isMenuActive{get;set;}
Item CurrentSelectedItem{get;set;}

	void Start () {
		ItemConteiner=Resources.Load<InventoryItemUI>("UI/Inventory/Item_Conteiner");
		UIEventHandler.OnItemAddedToInventory +=ItemAdded;
		InventoryPanel.gameObject.SetActive(false);
		isMenuActive=false;
	}
		
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.I)){
			isMenuActive=!isMenuActive;
			InventoryPanel.gameObject.SetActive(isMenuActive);
		}
	}
	public void ItemAdded(Item item){
		InventoryItemUI emptyItem=Instantiate(ItemConteiner);
		emptyItem.SetItem(item);
		emptyItem.transform.SetParent(ScrollviewContent);
	}	
}
