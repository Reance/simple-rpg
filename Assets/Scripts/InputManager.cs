﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

	[SerializeField] private RectTransform CharacterPanel;
	private bool isActive;
	// Use this for initialization
	void Start () {
		isActive=false;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.C)){
			isActive=!isActive;
			CharacterPanel.gameObject.SetActive(isActive);
			
		}
	}

}
