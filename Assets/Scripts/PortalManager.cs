﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PortalManager : MonoBehaviour {
    [SerializeField]
    private Button button;
    private Portal[] portals;
    private Player player;
    private GameObject Panel { get; set; }
	// Use this for initialization
	void Start () {
        player = FindObjectOfType<Player>();
        Panel = transform.Find("Panel_Portal").gameObject;

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ActivatePortal(Portal[] portals)
    {
        Panel.SetActive(true);
        for(int i = 0; i < portals.Length; i++)
        {
            Button portalButton = Instantiate(button, Panel.transform);
            portalButton.GetComponentInChildren<Text>().text = portals[i].name;
            int index = i;
            portalButton.onClick.AddListener(()=> OnPortalButtonClick(portals[index]));
        }
    }

    void OnPortalButtonClick(Portal portal)
    {
        player.transform.position = portal.TeleportLocation;

        foreach (Button button in GetComponentsInChildren<Button>())
        {
            Destroy(button.gameObject);
        }
        Panel.SetActive(false);
    }
}
