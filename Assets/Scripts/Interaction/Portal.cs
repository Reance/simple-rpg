﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : ActionItem {
    public Vector3 TeleportLocation{ get; set; }
    [SerializeField]
    private Portal[] linkedPortals;
    private PortalManager portalManager;

    void Start () {
        portalManager = FindObjectOfType<PortalManager>();
        TeleportLocation = transform.position + (Vector3.right * 5f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public override void Interact()
    {
        Debug.Log("interacting with portal:" + this.name);
        portalManager.ActivatePortal(linkedPortals);
        PlayerAgent.ResetPath();
    }
    
}
