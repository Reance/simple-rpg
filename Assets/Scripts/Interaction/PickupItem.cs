﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupItem : Interactable {
    public Item DropItem { get; set; }
    		
    public override void Interact()
    {
        InventoryManager.Instance.AddItem(DropItem);       
        Destroy(gameObject);
        
    }
}
