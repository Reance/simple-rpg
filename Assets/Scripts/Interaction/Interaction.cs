﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Interaction : MonoBehaviour {
    NavMeshAgent playerAgent;


	// Use this for initialization
	void Start () {
        playerAgent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0) && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
        {
            getInteraction();
        }
        Debug.DrawRay(transform.position,transform.forward*5f,Color.red);
	}

    void getInteraction()
    {
        Ray interactionRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit interactionInfo;
        if (Physics.Raycast(interactionRay, out interactionInfo, Mathf.Infinity))
        {
            playerAgent.updateRotation=true;
            GameObject interactedObject = interactionInfo.collider.gameObject;

            if (interactedObject.gameObject.tag == "Interactable Object")
            {
                interactedObject.GetComponent<Interactable>().GoToInteraction(this.playerAgent);
            }
            else if(interactedObject.gameObject.tag=="Enemy"){
                Debug.Log("moving to the enemy");
                interactedObject.GetComponent<Interactable>().GoToInteraction(this.playerAgent);
            }
            else 
            {
                playerAgent.stoppingDistance = 0f;
                playerAgent.destination = interactionInfo.point;
            }
        }

    }
}
