﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Interactable : MonoBehaviour {
    [HideInInspector]
    public NavMeshAgent PlayerAgent;
    private bool hasInteracted;
	 bool isEnemy;

    public virtual void GoToInteraction(NavMeshAgent playerAgent)
    {
        isEnemy = gameObject.tag=="Enemy";
        hasInteracted = false;
        this.PlayerAgent = playerAgent;
        PlayerAgent.stoppingDistance = 5f;
        PlayerAgent.destination = this.transform.position;                 
        
    }
      void Update()
    {
        if (PlayerAgent != null && !PlayerAgent.pathPending && !hasInteracted)
        {
            if (PlayerAgent.remainingDistance <= PlayerAgent.stoppingDistance)
            {
                if(!isEnemy)
                    Interact();
                EnsureLookDirection();
                hasInteracted = true;
            }
        }
    }
    void EnsureLookDirection(){
        PlayerAgent.updateRotation=false;
        Vector3 lookDirection =new Vector3(transform.position.x,PlayerAgent.transform.position.y,transform.position.z);
        PlayerAgent.transform.LookAt(lookDirection);
        PlayerAgent.updateRotation=true;
    }
    public virtual void Interact()
    {
        Debug.Log("interacting with base class.");
    }

}
