﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsumableManager : MonoBehaviour {

 CharacterStats characterStats;

 void Start()
 {
     characterStats=GetComponent<Player>().characterStats;
 }

 public void ConsumeItem(Item item){
     GameObject itemToSpawn=Instantiate(Resources.Load<GameObject>("Consumables/"+item.ItemID));

     if(item.isModifier)
     {
         itemToSpawn.GetComponent<IConsumable>().Consume(characterStats);
     }
     else
     {
         itemToSpawn.GetComponent<IConsumable>().Consume();
     }
 }
}
