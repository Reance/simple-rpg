﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
	public GameObject Enemy;
	public bool DoesRespawn;
	public float SpawnDelay;
	private bool spawning;
	private float currentTime;
	

	void Start()
	{
		Spawn();
		
	}
	private void Update() {
		if(spawning){
			currentTime-=Time.deltaTime;
			if(currentTime<=0){
				Spawn();
			}
		}

	}
	public void Respawn(){
		spawning=true;
		currentTime=SpawnDelay;
	}
	void Spawn(){

		IEnemy instance=Instantiate(Enemy,transform.position,Quaternion.identity).GetComponent<IEnemy>();
		instance.enemySpawner=this;
		spawning=false;
	}


	
}
