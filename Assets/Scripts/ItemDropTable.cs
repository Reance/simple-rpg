﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDropTable : MonoBehaviour {
	public List<Loot> LootDrops;

	public Item GetDrop(){
		int dropChanceSum=0;
		int roll=Random.Range(0,101);
		foreach(Loot drop in LootDrops){
			dropChanceSum +=drop.DropChance;
			if(roll<=dropChanceSum){
				return ItemDatabase.Instance.GetItem(drop.ItemID);
			}
		}
		return null;		
	}
}

public class Loot{
	public string ItemID { get; set; }
	public int DropChance { get; set; }

	public Loot(string itemID,int dropChance ){

		this.ItemID=itemID;
		this.DropChance=dropChance;
	}
	
}
