﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignPost : ActionItem {

    public string[] Dialogue;
    public string Name;
    public override void Interact()
    {
        DialogueManager.Instance.AddNewDialogue(Dialogue, Name);
        Debug.Log("Interacting with Sign Post.");
    }
}
