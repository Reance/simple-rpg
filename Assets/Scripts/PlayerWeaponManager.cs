﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeaponManager : MonoBehaviour {

 public GameObject PlayerHand;
 public GameObject EquipedWeapon { get; set; }
 IWeapon equipedWeapon;
 Transform projectileSpawn;
 CharacterStats characterStats;
Item currentEquipedWeapon;
  private void Start() {
	 characterStats=GetComponent<Player>().characterStats;
     projectileSpawn=transform.Find("ProjectileSpawn");
     
 }
 public void EquipWeapon(Item itemToEquip){
	 if(EquipedWeapon!=null){
         UnequipWeapon();
     }
    
     EquipedWeapon=(GameObject)Instantiate(Resources.Load("Weapons/"+itemToEquip.ItemID),PlayerHand.transform.position,PlayerHand.transform.rotation);
      equipedWeapon=EquipedWeapon.GetComponent<IWeapon>();
      if(EquipedWeapon.GetComponent<IProjectile>()!=null)
      EquipedWeapon.GetComponent<IProjectile>().ProjectileSpawn=projectileSpawn;
     equipedWeapon.Stats=itemToEquip.Stats;
     currentEquipedWeapon=itemToEquip;
     EquipedWeapon.transform.SetParent(PlayerHand.transform,false);
     characterStats.AddStatBonus(itemToEquip.Stats); 
     UIEventHandler.ItemEquipped(currentEquipedWeapon);
     UIEventHandler.UpdatePlayerStats();
    
 }
 public void UnequipWeapon(){
     InventoryManager.Instance.AddItem(currentEquipedWeapon.ItemID);                         
     characterStats.RemoveStatBonus(EquipedWeapon.GetComponent<IWeapon>().Stats);
     Destroy(PlayerHand.transform.GetChild(0).gameObject);
     UIEventHandler.UpdatePlayerStats();                  
 }
 private void Update() {
     if(Input.GetKeyDown(KeyCode.X))
        PerformBasicAttack();
    else if(Input.GetKeyDown(KeyCode.Z))
        PerformSpecialAttack();
 }
 public void PerformBasicAttack(){
     equipedWeapon.PerformAttack(calculateDamage());
 }
 public void PerformSpecialAttack(){
     equipedWeapon.PerformSpecialAttack();
 }
 private int calculateDamage(){
     int damageToDeal=characterStats.GetStat(BaseStat.BaseStatTypes.Power).GetModifiedStatValue();
     damageToDeal+=calculateCriticalDamage(damageToDeal);
     Debug.Log("Damage dealt:"+damageToDeal);
     return damageToDeal;
 }
 private int calculateCriticalDamage(int damage){//do real critical calculating in here with dex stat or item bonus..
     if(Random.value<=0.1f)
        return (int)(damage*Random.Range(.25f,1f));
    else
        return 0;
 }
}
