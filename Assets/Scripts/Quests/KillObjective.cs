﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillObjective : Objective {

	public int EnemyID { get; set; }

	public KillObjective(Quest quest,int enemyID,string description,bool completed,int completedAmount,int requiredAmount)
	{	
		this.Quest=quest;
		this.EnemyID=EnemyID;
		this.Completed=completed;
		this.Description=description;
		this.CompletedAmount=completedAmount;
		this.RequiredAmount=requiredAmount;
	}
	public override void Init(){
		base.Init();
		CombatEvents.OnEnemyDeath+=EnemyDied;
	}
	void EnemyDied(IEnemy enemy){

		if(EnemyID==enemy.ID){
			this.CompletedAmount++;
			Evaluate(); 
		}
	}
	
}
