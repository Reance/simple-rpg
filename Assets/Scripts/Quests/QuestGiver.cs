﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestGiver : NPC {
	public bool QuestAssinged { get; set; }
	public bool QuestCompleted { get; set; }
	[SerializeField] private GameObject quests;
	[SerializeField] private string questType;
	private Quest quest{get;set;}
	public override void Interact(){
		base.Interact();
		if(!QuestAssinged && !QuestCompleted){
			AssingQuest();
		}else if(QuestAssinged &&!QuestCompleted){
			CheckQuest();
		}else{
			DialogueManager.Instance.AddNewDialogue(new string[]{"Thank you!"},Name);
		}
	}
	void AssingQuest(){
		QuestAssinged=true;
		quest=(Quest)quests.AddComponent(System.Type.GetType(questType));

	}
	void CheckQuest(){
		if(quest.Completed){
			quest.GiveReward();
			QuestCompleted=true;
			DialogueManager.Instance.AddNewDialogue(new string[]{"Thank you!! Here these are for your trouble"},Name);
		}
		else{
			DialogueManager.Instance.AddNewDialogue(new string[]{"What are you doing here? Get back to your work now!"},Name);
		}
	}
}
