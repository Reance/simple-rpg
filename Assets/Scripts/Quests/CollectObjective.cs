﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectObjective : Objective {

	public string ItemID { get; set; }

	public CollectObjective(Quest quest,string itemID,string description,bool completed,int completedAmount,int requiredAmount)
	{	
		this.Quest=quest;
		this.ItemID=itemID;
		this.Completed=completed;
		this.Description=description;
		this.CompletedAmount=completedAmount;
		this.RequiredAmount=requiredAmount;
	}
	public override void Init(){
		base.Init();
		UIEventHandler.OnItemAddedToInventory+=ItemPickedUp;

	}
	void ItemPickedUp(Item item){

		if(ItemID==item.ItemID){
			this.CompletedAmount++;
			Evaluate(); 
		}
	}
	
}
