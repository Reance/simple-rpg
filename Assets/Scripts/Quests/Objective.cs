﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objective  {
	public Quest Quest { get; set; }	
	public string Description { get; set; }
	public bool Completed { get; set; }
	public int CompletedAmount { get; set; }
	public int RequiredAmount{ get; set; }
	
	public virtual void Init(){
		//default init stuff..
	}
	public void Evaluate(){
		if(CompletedAmount>=RequiredAmount){
			Complete();
		}
	}
	void Complete(){
		Completed=true;
		Quest.CheckObjectives();
        Debug.Log(Description + " completed");
	}
	
}
