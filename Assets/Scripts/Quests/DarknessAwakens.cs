﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarknessAwakens : Quest {

	// Use this for initialization
	void Start () {
        this.Objectives = new List<Objective>();
        this.QuestName="Darkness Awakens";
		this.QuestDescription="Kill these motherfuckers.";
		this.ExpReward=200;
		this.ItemReward=ItemDatabase.Instance.GetItem("potion");

		this.Objectives.Add(new KillObjective(this,0,"Kill 2 bastard",false,0,2));
		this.Objectives.Add(new KillObjective(this,1,"Kill 2 bastard",false,0,2));
        this.Objectives.Add(new CollectObjective(this, "potion", "Find a potion for a sick man", false, 0, 1));
		this.Objectives.ForEach(x=>x.Init());
		
	}
	
}
