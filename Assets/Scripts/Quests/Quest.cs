﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class Quest : MonoBehaviour {

	public List<Objective> Objectives { get; set; }	
	public string QuestName { get; set; }
	public string QuestDescription { get; set; }
	public int ExpReward { get; set; }
	public Item ItemReward { get; set; }
	public bool Completed { get; set; }
	
	public void CheckObjectives(){
	 Completed=Objectives.All(x=>x.Completed);
	 //if(Completed) GiveReward();	 	
	}
	public void GiveReward(){
		if(ItemReward!=null){
			InventoryManager.Instance.AddItem(ItemReward);
		}
		EventManager.CompleteQuest(this);
		
	}
	
	void Start()
	{
		
	}

	
}
