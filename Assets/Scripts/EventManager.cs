﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour {

	public delegate void QuestEventHandler(Quest quest);
    
	public static event QuestEventHandler OnQuestCompleted;
  

	public static void CompleteQuest(Quest quest){
		OnQuestCompleted(quest);
	}
   

}
