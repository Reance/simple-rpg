﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour,IWeapon {
    public List<BaseStat> Stats{get;set;}

    public int currentDamage{get;set;}

    private Animator animator;
      
    void Start()
    {
        animator=GetComponent<Animator>();
        
        
    }
    public void PerformAttack(int damage)
    {   
        currentDamage=damage;
        animator.SetTrigger("Basic_Attack");
    }
    public void PerformSpecialAttack(){
        animator.SetTrigger("Special_Attack");
    }
    private void OnTriggerEnter(Collider other) {
        if(other.tag=="Enemy"){
            other.GetComponent<IEnemy>().TakeDamage(currentDamage);
        }
    }
   


}
