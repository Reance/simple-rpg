﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Staff : MonoBehaviour,IWeapon,IProjectile {
    public List<BaseStat> Stats{get;set;}

    public Transform ProjectileSpawn{get;set;}

    public int currentDamage{get;set;}
   

    Fireball fireball;

    private Animator animator;     
    void Start()
    {
        animator=GetComponent<Animator>();
        fireball=Resources.Load<Fireball>("Weapons/Projectiles/Fireball");
        
    }
    public void PerformAttack(int damage)
    {
        
        animator.SetTrigger("Basic_Attack");
        Debug.Log("basic_attack");
    }
    public void PerformSpecialAttack(){
        animator.SetTrigger("Special_Attack");
    }
 
    public void CastProjectile()
    {
       Fireball fireballInstance=(Fireball)Instantiate(fireball,ProjectileSpawn.position,transform.rotation);
       fireballInstance.Direction=ProjectileSpawn.forward;
    }

}
