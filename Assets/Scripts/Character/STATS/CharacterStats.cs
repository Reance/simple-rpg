﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats  {

    public List<BaseStat> Stats;
    public CharacterStats(int power,int vitality,int attackSpeed){
        Stats=new List<BaseStat>(){
            new BaseStat(BaseStat.BaseStatTypes.Power,power,"Power"),
            new BaseStat(BaseStat.BaseStatTypes.Vitality,vitality,"Vitality"),
            new BaseStat(BaseStat.BaseStatTypes.AttackSpeed,attackSpeed,"AtkSpd")
        };
    }
    public BaseStat GetStat(BaseStat.BaseStatTypes _statType){
        return this.Stats.Find(x=>x.StatType==_statType);
    }
    public void AddStatBonus(List<BaseStat> statbonuses){
        foreach(BaseStat statBonus in statbonuses){
            Debug.Log(GetStat(statBonus.StatType).StatName+" before adding bonus "+GetStat(statBonus.StatType).GetModifiedStatValue());
            GetStat(statBonus.StatType).AddStatBonus(new StatBonus(statBonus.BaseValue));
            Debug.Log(GetStat(statBonus.StatType).StatName+" after adding bonus "+GetStat(statBonus.StatType).GetModifiedStatValue());
        }
    }
    public void RemoveStatBonus(List<BaseStat> statbonuses){
        foreach(BaseStat statBonus in statbonuses){
            Debug.Log(GetStat(statBonus.StatType).StatName+" before removing bonus "+GetStat(statBonus.StatType).GetModifiedStatValue());
            GetStat(statBonus.StatType).RemoveStatBonus(new StatBonus(statBonus.BaseValue));
             Debug.Log(GetStat(statBonus.StatType).StatName+" after removing bonus "+GetStat(statBonus.StatType).GetModifiedStatValue());
        }
    }
}
