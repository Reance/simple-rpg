﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
public class BaseStat
{
    public enum BaseStatTypes{Power,Vitality,AttackSpeed}
    public List<StatBonus> StatModifiers  { get; set; }
    [JsonConverter(typeof(StringEnumConverter))]
    public BaseStatTypes StatType{get;set;}
    public int BaseValue { get; set; }
    public string StatName { get; set; }
    public string StatDescription { get; set; } 
    public int FinalValue{ get; set; }

    public BaseStat(int baseValue, string statName ,string statDescription)
    {
        this.StatModifiers = new List<StatBonus>();
        this.BaseValue = baseValue;
        this.StatName = statName;
        this.StatDescription = statDescription;
    }
    [Newtonsoft.Json.JsonConstructor]
     public BaseStat(BaseStatTypes statType,int baseValue, string statName)
    {
        this.StatModifiers = new List<StatBonus>();
        this.BaseValue = baseValue;
        this.StatName = statName;       
        this.StatType=statType;
    }
    public void AddStatBonus(StatBonus statBonus)
    {
        StatModifiers.Add(statBonus);
    }
    public void RemoveStatBonus(StatBonus statBonus)
    {
        foreach(var x in StatModifiers){
            Debug.Log("bonus stat to "+StatName+": "+x.BonusValue);
        }
        StatModifiers.ForEach(x=>
        {
            if(x.BonusValue==statBonus.BonusValue){
                StatModifiers.Remove(x);
            }
            
        });
        
        
        foreach(var x in StatModifiers){
            Debug.Log("after remove bonus stat to "+StatName+": "+x.BonusValue);
        }
        
        
    }
    public int GetModifiedStatValue()
    {
        this.FinalValue = 0;
        StatModifiers.ForEach(i => FinalValue+=i.BonusValue);
        FinalValue += BaseValue;
        return FinalValue;
    }
}
