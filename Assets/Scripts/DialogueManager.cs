﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DialogueManager : MonoBehaviour {

    public static DialogueManager Instance{ get; set; }

    public GameObject DialoguePanel;
    public List<string> DialogueLines=new List<string>();
    public string NpcName;

    Button continueButton;
    Text dialogueText, nameText;
    int dialogueIndex;
    void Awake () {
        continueButton = DialoguePanel.transform.Find("ContinueButton").GetComponent<Button>();        
        dialogueText = DialoguePanel.transform.Find("Text").GetComponent<Text>();
        nameText = DialoguePanel.transform.Find("Name").GetChild(0).GetComponent<Text>();
        continueButton.onClick.AddListener(delegate { ContinueDialogue(); });
        DialoguePanel.SetActive(false);

		if(Instance!=null && Instance != this) 
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
	}
	
	 public void AddNewDialogue(string [] lines,string npcName)
    {
        dialogueIndex = 0;
        DialogueLines = new List<string>(lines.Length);
        DialogueLines.AddRange(lines);
        NpcName = npcName;
        CreateDialogue();
        
    }
    public void CreateDialogue()
    {
        dialogueText.text = DialogueLines[dialogueIndex];
        nameText.text = NpcName;
        DialoguePanel.SetActive(true);

    }
    public void ContinueDialogue()
    {
        if (dialogueIndex >= DialogueLines.Count-1)
        {
            DialoguePanel.SetActive(false);
        }
        else
        {
            dialogueIndex++;
            dialogueText.text = DialogueLines[dialogueIndex];
        }
        
    }
}
